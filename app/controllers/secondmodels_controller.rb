class SecondmodelsController < ApplicationController
  before_action :set_secondmodel, only: [:show, :edit, :update, :destroy]

  # GET /secondmodels
  # GET /secondmodels.json
  def index
    @secondmodels = Secondmodel.all
  end

  # GET /secondmodels/1
  # GET /secondmodels/1.json
  def show
  end

  # GET /secondmodels/new
  def new
    @secondmodel = Secondmodel.new
  end

  # GET /secondmodels/1/edit
  def edit
  end

  # POST /secondmodels
  # POST /secondmodels.json
  def create
    @secondmodel = Secondmodel.new(secondmodel_params)

    respond_to do |format|
      if @secondmodel.save
        format.html { redirect_to @secondmodel, notice: 'Secondmodel was successfully created.' }
        format.json { render :show, status: :created, location: @secondmodel }
      else
        format.html { render :new }
        format.json { render json: @secondmodel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /secondmodels/1
  # PATCH/PUT /secondmodels/1.json
  def update
    respond_to do |format|
      if @secondmodel.update(secondmodel_params)
        format.html { redirect_to @secondmodel, notice: 'Secondmodel was successfully updated.' }
        format.json { render :show, status: :ok, location: @secondmodel }
      else
        format.html { render :edit }
        format.json { render json: @secondmodel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /secondmodels/1
  # DELETE /secondmodels/1.json
  def destroy
    @secondmodel.destroy
    respond_to do |format|
      format.html { redirect_to secondmodels_url, notice: 'Secondmodel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_secondmodel
      @secondmodel = Secondmodel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def secondmodel_params
      params.require(:secondmodel).permit(:name, :phone)
    end
end
