class ThirdmodelsController < ApplicationController
  before_action :set_thirdmodel, only: [:show, :edit, :update, :destroy]

  # GET /thirdmodels
  # GET /thirdmodels.json
  def index
    @thirdmodels = Thirdmodel.all
  end

  # GET /thirdmodels/1
  # GET /thirdmodels/1.json
  def show
  end

  # GET /thirdmodels/new
  def new
    @thirdmodel = Thirdmodel.new
  end

  # GET /thirdmodels/1/edit
  def edit
  end

  # POST /thirdmodels
  # POST /thirdmodels.json
  def create
    @thirdmodel = Thirdmodel.new(thirdmodel_params)

    respond_to do |format|
      if @thirdmodel.save
        format.html { redirect_to @thirdmodel, notice: 'Thirdmodel was successfully created.' }
        format.json { render :show, status: :created, location: @thirdmodel }
      else
        format.html { render :new }
        format.json { render json: @thirdmodel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /thirdmodels/1
  # PATCH/PUT /thirdmodels/1.json
  def update
    respond_to do |format|
      if @thirdmodel.update(thirdmodel_params)
        format.html { redirect_to @thirdmodel, notice: 'Thirdmodel was successfully updated.' }
        format.json { render :show, status: :ok, location: @thirdmodel }
      else
        format.html { render :edit }
        format.json { render json: @thirdmodel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /thirdmodels/1
  # DELETE /thirdmodels/1.json
  def destroy
    @thirdmodel.destroy
    respond_to do |format|
      format.html { redirect_to thirdmodels_url, notice: 'Thirdmodel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_thirdmodel
      @thirdmodel = Thirdmodel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def thirdmodel_params
      params.require(:thirdmodel).permit(:name)
    end
end
