json.array!(@secondmodels) do |secondmodel|
  json.extract! secondmodel, :id, :name, :phone
  json.url secondmodel_url(secondmodel, format: :json)
end
