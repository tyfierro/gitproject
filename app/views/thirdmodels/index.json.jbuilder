json.array!(@thirdmodels) do |thirdmodel|
  json.extract! thirdmodel, :id, :name
  json.url thirdmodel_url(thirdmodel, format: :json)
end
