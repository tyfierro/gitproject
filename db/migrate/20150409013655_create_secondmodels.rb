class CreateSecondmodels < ActiveRecord::Migration
  def change
    create_table :secondmodels do |t|
      t.string :name
      t.string :phone

      t.timestamps null: false
    end
  end
end
