class CreateThirdmodels < ActiveRecord::Migration
  def change
    create_table :thirdmodels do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
