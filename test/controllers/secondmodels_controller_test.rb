require 'test_helper'

class SecondmodelsControllerTest < ActionController::TestCase
  setup do
    @secondmodel = secondmodels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:secondmodels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create secondmodel" do
    assert_difference('Secondmodel.count') do
      post :create, secondmodel: { name: @secondmodel.name, phone: @secondmodel.phone }
    end

    assert_redirected_to secondmodel_path(assigns(:secondmodel))
  end

  test "should show secondmodel" do
    get :show, id: @secondmodel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @secondmodel
    assert_response :success
  end

  test "should update secondmodel" do
    patch :update, id: @secondmodel, secondmodel: { name: @secondmodel.name, phone: @secondmodel.phone }
    assert_redirected_to secondmodel_path(assigns(:secondmodel))
  end

  test "should destroy secondmodel" do
    assert_difference('Secondmodel.count', -1) do
      delete :destroy, id: @secondmodel
    end

    assert_redirected_to secondmodels_path
  end
end
