require 'test_helper'

class ThirdmodelsControllerTest < ActionController::TestCase
  setup do
    @thirdmodel = thirdmodels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:thirdmodels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create thirdmodel" do
    assert_difference('Thirdmodel.count') do
      post :create, thirdmodel: { name: @thirdmodel.name }
    end

    assert_redirected_to thirdmodel_path(assigns(:thirdmodel))
  end

  test "should show thirdmodel" do
    get :show, id: @thirdmodel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @thirdmodel
    assert_response :success
  end

  test "should update thirdmodel" do
    patch :update, id: @thirdmodel, thirdmodel: { name: @thirdmodel.name }
    assert_redirected_to thirdmodel_path(assigns(:thirdmodel))
  end

  test "should destroy thirdmodel" do
    assert_difference('Thirdmodel.count', -1) do
      delete :destroy, id: @thirdmodel
    end

    assert_redirected_to thirdmodels_path
  end
end
